#!/bin/bash
    
#Variaveis
DEPS_PACKAGES="apt-transport-https ca-certificates curl gnupg-agent software-properties-common python3 python3-pip vim openjdk-11-jre wget tree apt-transport-https ca-certificates curl gnupg lsb-release"
PACKAGES="docker-ce docker-ce-cli containerd.io"
PIP_PACKAGES="ComplexHTTPServer ansible"

# Registrando timestamp provision
sudo date >> /var/log/vagrant_provision.log

validateCommand() {
  if [ $? -eq 0 ]; then
    echo "[OK] $1"
  else
    echo "[ERROR] $1"
    exit 1
  fi
}

# Setando permissão no arquivo de logs
sudo chown vagrant: /var/log/vagrant_provision.log

# Instalando Pacotes
export DEBIAN_FRONTEND=noninteractive
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get --allow-releaseinfo-change update -qq >/dev/null 2>>/var/log/vagrant_provision.log && \
  sudo apt-get update -qq -y >/dev/null 2>>/var/log/vagrant_provision.log && \
	sudo apt-get install -qq -y ${DEPS_PACKAGES} ${PACKAGES} >/dev/null 2>>/var/log/vagrant_provision.log

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose >/dev/null 2>>/var/log/vagrant_provision.log
sudo chmod +x /usr/local/bin/docker-compose

validateCommand "Instalacao de Pacotes"

# Instalando Pacotes do Python
pip3 install -q ${PIP_PACKAGES} >/dev/null 2>>/var/log/vagrant_provision.log && \
        pip3 uninstall -y docker-py >/dev/null 2>>/var/log/vagrant_provision.log

validateCommand "Pacotes Python"

# Configurando logs via Gelf no Docker
cat <<EOF >>/etc/docker/daemon.json
{
  "log-driver": "gelf",
  "log-opts": {
    "gelf-address": "udp://192.168.159.10:12201"
  }
}
EOF

validateCommand "Configurando Logs do Docker"

#Ativando Servico do Docker
systemctl enable docker &>/dev/null && \
        systemctl start docker

validateCommand "Ativando Docker e configurando logs"

#Instalando Logstash
cd /tmp &>/dev/null && \
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add - &>/dev/null && \
sudo apt-get install apt-transport-https &>/dev/null && \
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list &>/dev/null && \
sudo apt-get update &>/dev/null && \ 
sudo apt-get install logstash &>/dev/null && \
cd - &>/dev/null

validateCommand "Instalando Logstash"

#Subindo container Portainer
docker volume create portainer_data
docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce

validateCommand "Subindo portainer"