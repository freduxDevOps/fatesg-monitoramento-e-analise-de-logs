# FATESG - Monitoramento e análise de logs

O presente repositório conta com os arquivos utilizados na disciplina de Monitoramento e Análise de Logs do SENAI FATESG, ministrado pelo professor Especialista Samuel Gonçalves Pereira

Os arquivos estão organizados em diretórios, e cada diretório possui configurações específicas de cada aplicação. Como podemos perceber na seguinte listagem:

* **Configurações**: Contém subdiretórios contendo as configurações dos softwares `nginx`, `logstash`, `filebeat` e `prometheus`, com a seguinte estrutura:
```shell
├── [  70]  configuracoes
│   ├── [ 429]  filebeat.yml
│   ├── [  38]  logstash
│   │   ├── [ 740]  gelf.conf
│   │   └── [ 585]  nginx.conf
│   ├── [  20]  nginx
│   │   └── [ 185]  juice.conf
│   └── [  28]  prometheus
│       └── [ 563]  prometheus.yml
```
* **Scripts**: Esta pasta contem o script de provisionamento da máquina com Vagrant e/ou em qualquer provedor cloud.
* **Stacks**: Este diretório contém os arquivos de stacks que serão utilizados no `portainer`.
* **`machines.yml`**: arquivo utilizado para subir o laboratório com Vagrant
* **`Vagrantfile`**: Arquivo de configuração para utilização do laboratório com Vagrant.

## Detalhes para uso do laboratório local com Vagrant

Para utilizar o laboratório localmente e montar tudo utilizando o Vagrant é simples. Siga os passos para executar o lab automáticamente:

1. Primeiro, clone o repositório:
```shell
git clone https://gitlab.com/sg-wolfgang/fatesg-monitoramento-e-analise-de-logs.git
```
2. Acesse o diretório clonado:
```shell
cd fatesg-monitoramento-e-analise-de-logs
```
3. Execute o ambiente com vagrant:
```shell
vagrant up
```

Todo conteúdo presente neste repositório visa única e exclusivamente o ensino, por isso, use-o da melhor maneira que puder!

Em caso de dúvida entrar em contato com Samuel Gonçalves via e-mail: samuel@sgoncalves.tec.br